Hello, Welcome to my project "rn-marvel-app".

1- Clone project : git clone https://gitlab.com/amer.toumi/rn_marvel_app/
2- Install project : npm install

--------------------------
** Connect to Marvel API :
---------------------------
To connect your api you can use the '.env' file and insert your credentials (API key , Secret Key , Hash and Ts). or import them by the 'react-native-dotenv' package.
or you can introduce directly them on the  'service/api.js' file .

1- apiKey = PUBLIC_KEY = 'import it from developer.marvel.com'
2- PRIVATE_KEY = 'import it from developer.marvel.com'
3- timestamp = 1
4- For the Hash key you can use a tool for creating an MD5 hash from a string 
 Hash = (timestamp + PRIVATE_KEY + PUBLIC_KEY)

!! 
Example for MD5 generator  : https://www.md5hashgenerator.com/

Thanks.
by Amer Toumi
Email: amer.toumi@gmail.com
