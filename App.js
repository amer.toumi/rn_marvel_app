import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {StyleSheet} from 'react-native';
import SearchScreen from './src/screens/SearchScreen';
import Detail from './src/screens/Detail';
import Logo from './src/components/Logo';

const AppNavigator = createStackNavigator(
  {
    SearchScreen,
    Detail,
  },
  {
    defaultNavigationOptions: {
      headerTitle: () => <Logo style={styles.logo}/>,
      headerStyle: {
        borderBottomWidth: 0,
      },
      headerTintColor: '#e71a24',
    },
    headerLayoutPreset: 'center',
  },
);

//style
const styles = StyleSheet.create({
  logo: {
    width: 200
  }
})

export default createAppContainer(AppNavigator);
