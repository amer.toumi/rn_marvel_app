import React, { useState, useEffect } from 'react';
import { View, ImageBackground, ActivityIndicator, Text, StyleSheet, Button } from 'react-native';
import char from '../pages/3dman.json';
import api from '../services/api.js';

const Detail = ({ navigation }) => {
  const [character, setCharacter] = useState(char);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const id = navigation.getParam('id');
    async function loadCharacter() {
      const response = await api.get(`/v1/public/characters/${id}`);
      const [char] = response.data.data.results;
      setCharacter(char);
      setLoading(false);
    }

    loadCharacter();
  }, []);

  if (loading) {
    return (
      <View style={styles.viewLoading}>
        <ActivityIndicator color="red" />
      </View>
    );
  }

  const [detail, wiki, comics] = character.urls;

  return (
    <View style={styles.view}>
      <ImageBackground
        style={styles.imgageBackground}
        source={{
          uri: `${character.thumbnail.path}.${character.thumbnail.extension}`,
        }}
        resizeMode="cover"
      >
        <View style={{ alignItems: 'center' }}>
          <View
            style={styles.viewTextName}
          >
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                {character.name}
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

// Styles
const styles = StyleSheet.create({
  viewLoading: { 
    flex: 1, alignItems: 'center', justifyContent: 'center'
  },
  view:{ 
    flex: 1 
  },
  imgageBackground: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingBottom: 25,
  },
  viewTextName: {
    backgroundColor: '#fff',
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    paddingHorizontal: 20,
    borderRadius: 20,
  },
  viewIconbutton: {
     flexDirection: 'row' 
  },


});

export default Detail;
