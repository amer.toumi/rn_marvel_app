import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const Input = props => {
  return (
    <View style={styles.view} >

      <Icon name="search1" size={18} color="red" />
      <TextInput
        style={styles.textInput}
        autoCorrect={false}
        placeholder="Choose your hero"
        placeholderTextColor="black"
        returnKeyType="search"
        selectTextOnFocus
        {...props}
      />

    </View>
  );
};

//Styles
const styles = StyleSheet.create({
  view: {
      backgroundColor: 'white',
      borderRadius: 20,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 15,
      marginHorizontal: 20,
    
  },
  textInput: {
    marginLeft: 10,
    height: 40,
    flex: 1,
    color: 'black',
  }
});

export default Input;
