import React from 'react';
import { Image, StyleSheet } from 'react-native';

const Logo = () => {
  return (
    <Image
      style={styles.image}
      resizeMode="contain"
      source={require('../images/logo.png')}
    />
  );
};

//Styles
const styles = StyleSheet.create({
  image: {
    height: 50
  }
});

export default Logo;
