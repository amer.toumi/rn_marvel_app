import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

const Hero = ({ item, navigation }) => {
  return (
    <View
      style={styles.wrapperView}
    >
      <RectButton
        style={styles.rectButton}
        onPress={() => navigation.navigate('Detail', { id: item.id })}
      >
        <Image
          style={styles.image}
          resizeMode="cover"
          source={{ uri: `${item.thumbnail.path}.${item.thumbnail.extension}` }}
        />
        <View style={styles.view}>
          <View>
            <Text style={styles.textName}>
              {item.name}
            </Text>
            <Text style={styles.textInfo}>
              {item.comics.items.length} Comics |{' '}
              {item.stories.items.length} Stories
            </Text>
            <Text>price :{item.prices} $</Text>
          </View>
          <View>
            <Text style={styles.textDesc} numberOfLines={3}>
              {item.description
                ? item.description
                : 'Character has no description yet'}
            </Text>
          </View>
          <Text
            style={styles.textViewMore}
          >
            View More
          </Text>
        </View>
      </RectButton>
    </View>
  );
};

//Styles
const styles = StyleSheet.create({
  image: {
    height: 150,
    width: 100,
    backgroundColor: '#ccc',
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
  },
  wrapperView: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowRadius: 10,
    shadowOpacity: 0.2,
  },
  rectButton: {
    borderRadius: 20,
    backgroundColor: '#fff',
    marginHorizontal: 20,
    flexDirection: 'row',
    marginBottom: 15,
    elevation: 5,
  },
  view: {
    padding: 10, flex: 1, justifyContent: 'space-between' 
  },
  textName: { 
    fontSize: 18, fontWeight: 'bold', marginBottom: 2 
  },
  textInfo: { 
    fontSize: 10, color: 'rgba(0,0,0,.6)' 
  },
  textDesc: { 
    fontSize: 12, marginVertical: 5 
  },
  textViewMore: {
    color: '#e71a24',
  }


});

export default Hero;
