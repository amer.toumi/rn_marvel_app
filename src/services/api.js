import axios from 'axios';
//import { API_KEY, HASH, TS } from 'react-native-dotenv';
//Insert your credencial here :

const API_KEY = '';
const HASH = ''
const TS = '1';

const api = axios.create({
  baseURL: 'https://gateway.marvel.com',
});

api.interceptors.request.use(config => {

  config.params = config.params || {};
  config.params.apikey = API_KEY;
  config.params.hash = HASH;
  config.params.ts = TS;
  return config;
});

export default api;